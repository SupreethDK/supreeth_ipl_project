let teamsMatchAndTossWin = (matches) => matches.filter(match => (match.winner === match.toss_winner)).map(team => team.winner).reduce((output,cv) => {output[cv] ? output[cv]++ : output[cv] = 1; return output}, {});

let playerHighestMatchAwardsEachSeason = (matches) => {
  let playersOfMatchPerSeason = matches.reduce((acc, match) => { let season = match.season
  if(!acc[season]) {
    acc[season] = []
  } else {
    acc[season].push(match.player_of_match);
  }; return acc},{});

  Object.entries(playersOfMatchPerSeason).forEach(([key,value]) => {
    let topPlayer = value.sort((a,b) => value.filter(v => v===a).length- value.filter(v => v===b).length).pop();
    playersOfMatchPerSeason[key] = topPlayer;
  })
  return playersOfMatchPerSeason;
}

let strikeRateOfBatsmanPerSeason = (matches, deliveries) => {
  let matchesSeasonId = matches.reduce((acc, match) => { let season = match.season
      if(!acc[season]) {
        acc[season] = []
      } else {
        acc[season].push(match.id);
      }; return acc},{});
  let batsmanPerSeason;
  Object.entries(matchesSeasonId).forEach(([key,value]) => {
    batsmanPerSeason = deliveries.reduce((acc, delivery) => {
      if(value.includes(delivery.match_id)) {
      if(!acc[delivery.batsman]) {
        acc[delivery.batsman] = {};
        acc[delivery.batsman].balls = 1;
        acc[delivery.batsman].runs = Number(delivery.batsman_runs);
    } else {
        acc[delivery.batsman].balls += 1;
        acc[delivery.batsman].runs += Number(delivery.batsman_runs);
    }
  }
    return acc;
  }, {} );
  matchesSeasonId[key] = batsmanPerSeason;
  })

Object.entries(matchesSeasonId).forEach(([key, value]) => {
  let batsmanList = value; 
  matchesSeasonId[key] = cb(batsmanList);
})


function cb(object) {
  Object.entries(object).forEach(([key,value]) => {
    let strikeRate = (value.runs/value.balls)*100;
    object[key] = strikeRate.toFixed(2);
  })
  return object
}


return matchesSeasonId;
}


let bestEconomyBowlerInSuperOver = (deliveries) => {
  let bowlerDictionary = deliveries.reduce((acc, delivery) => {
    if(delivery.is_super_over === "1") {
      if(!acc[delivery.bowler]) {
        acc[delivery.bowler] = {};
        acc[delivery.bowler].balls = 1;
        acc[delivery.bowler].runs = Number(delivery.total_runs);
      } else {
        acc[delivery.bowler].balls += 1;
        acc[delivery.bowler].runs += Number(delivery.total_runs);
      }
    }
    return acc;
  }, {});

  Object.entries(bowlerDictionary).forEach(([key,object]) => {
    let economy = object.runs / (object.balls/6);
    bowlerDictionary[key] = economy.toFixed(2);
  })

  let output = {};
  Object.entries(bowlerDictionary).sort((a,b) => a[1] - b[1]).forEach(item => output[item[0]] = item[1])
  return output;
}


let highestDismissedPlayer = (deliveries) => {
  let playerObject = deliveries.reduce((acc,delivery) => {
    if (delivery.dismissal_kind.length > 1) {
      if(!acc[delivery.batsman]) {
        acc[delivery.batsman] = 1;
      } else {
        acc[delivery.batsman] += 1;
      }
    };
    return acc;
  },{});

  let output = {}
  Object.entries(playerObject).sort((a,b) => b[1] - a[1]).slice(0,11).forEach(item => output[item[0]] = item[1]);
  return output;
}


module.exports = {
  teamsMatchAndTossWin,
  playerHighestMatchAwardsEachSeason,
  strikeRateOfBatsmanPerSeason,
  bestEconomyBowlerInSuperOver,
  highestDismissedPlayer
};
