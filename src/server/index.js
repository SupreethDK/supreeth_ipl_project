const fs = require("fs");
const csv = require("csvtojson");
const iplFunctions = require("./ipl.js");

const matchesFile = "../data/matches.csv";
const deliveriesFile = "../data/deliveries.csv";

const teamMatchAndTossWinJSON = "../output/teamsMatchAndTossWin.json";
const playerHighestMatchAwardsEachSeasonJSON = "../output/playerHighestMatchAwardsEachSeason.json";
const strikeRateOfBatsmanPerSeasonJSON = "../output/strikeRateOfBatsmanPerSeason.json";
const bestBowlerWithBestEconomyInSuperOverJSON = "../output/bestEconomyBowlerInSuperOver.json";
const highestDismissedPlayerJSON = "../output/highestDismissedPlayer.json";

const outputDir = '../output';
if (!fs.existsSync(outputDir)){
    fs.mkdirSync(outputDir);
}

function main() {
  csv()
    .fromFile(matchesFile)
    .then((matches) => {
      csv()
        .fromFile(deliveriesFile)
        .then((deliveries) => {
          write_File({teamsMatchAndTossWin: iplFunctions.teamsMatchAndTossWin(matches)},teamMatchAndTossWinJSON).catch((err) => console.error("error in writing file occurred", err))
          write_File({playerHighestMatchAwardsEachSeason: iplFunctions.playerHighestMatchAwardsEachSeason(matches)},playerHighestMatchAwardsEachSeasonJSON).catch((err) => console.error("error in writing file occurred", err))
          write_File({strikeRateOfBatsmanPerSeason: iplFunctions.strikeRateOfBatsmanPerSeason(matches,deliveries)},strikeRateOfBatsmanPerSeasonJSON).catch((err) => console.error("error in writing file occurred", err))
          write_File({bestEconomyBowlerInSuperOver: iplFunctions.bestEconomyBowlerInSuperOver( deliveries)},bestBowlerWithBestEconomyInSuperOverJSON).catch((err) => console.error("error in writing file occurred", err))
          write_File({highestDismissedPlayer: iplFunctions.highestDismissedPlayer(deliveries)},highestDismissedPlayerJSON).catch((err) => console.error("error in writing file occurred", err))
        })
    })
    .catch((err) => console.error(err));
}

let write_File = (jsonData, jsonPath) => {
  const jsonString = JSON.stringify(jsonData);
  return new Promise ((resolve,reject) => {
    fs.writeFile(jsonPath, jsonString, "utf8", (err) => {
        if(err) {
          reject(err);
        }
    })
})
}

main();